(* arrays with private types for keys, so that we are forced to use
   the correct injection function and can't mix up keys of arrays of
   different kind by mistake.

   The array type itself is also made private so that arr.(x) doesn't
   type -- that would allow to bypass the type-checking of `get`.
*)
module type ArraySig = sig
  type key = private int
  type 'a t = private 'a array
      
  val key : int -> key
  val array : 'a array -> 'a t

  val length : 'a t -> int
  val get : 'a t -> key -> 'a
  val set : 'a t -> key -> 'a -> unit
  val make : int -> 'a -> 'a t
  val create : int -> 'a -> 'a t
  val init : int -> (key -> 'a) -> 'a t
  val append : 'a t -> 'a t -> 'a t
  val concat : 'a t list -> 'a t
  val to_list : 'a t -> 'a list
  val of_list : 'a list -> 'a t
  val iter : ('a -> unit) -> 'a t -> unit
  val map : ('a -> 'b) -> 'a t -> 'b t
  val iteri : (key -> 'a -> unit) -> 'a t -> unit
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b t -> 'a
  val fold_right : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val unsafe_get : 'a t -> key -> 'a
  val unsafe_set : 'a t -> key -> 'a -> unit
end

(* the functor here allows to generate several instantiations with
   fresh (incompatible) key types; it does not constrain arrays to be
   polymorphic. *)
module ArrayMake (U : sig end) : ArraySig = struct
  type key = int
  type 'a t = 'a array
      
  let key x = x
  let array t = t

  include Array
end

(* by using a "struct end" signature instead of a named module as
   parameter, we ensure generativity: types for A1 and A2 are
   incompatible *)
module A1 = ArrayMake(struct end)
module A2 = ArrayMake(struct end)

let () = assert (A1.get (A1.make 3 true) (A1.key 2));;
let () = assert (A2.get (A2.make 3 true) (A2.key 2));;
let compile_fail = 
  (* fails as it should *)
  A1.get (A1.make 3 true) (A2.key 2);;
